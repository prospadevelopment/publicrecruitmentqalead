﻿using System;
using Humanizer;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorServices;

namespace Prospa.ChequeCreator.Services.CurrencyTranslatorServices
{
    public class CurrencyTranslatorServices : ICurrencyTranslatorServices
    {
        public string CapitalizeAmount(string amount)
        {
            var lower = amount.Trim();
            var lastPart = lower.Substring(1, lower.Length - 1);
            var firstPart = lower.Substring(0, 1);
            return string.Format("{0}{1}", firstPart.ToUpper(), lastPart.ToLower());
        }

        public int GetDollars(decimal amount)
        {
            return (int)Math.Floor(amount);
        }

        public int GetCents(decimal amount)
        {
            return (int) ((amount % 1) * 100);
        }

        public string PluralizeDenomination(string denomination, int value)
        {
            return value == 1 ? denomination : denomination.Pluralize();
        }

        public string GetIntegerAsWords(int value)
        {
            return value.ToWords();
        }
    }
}
