### Welcome ###

This is the repo to apply for the role "Quality engineer - automation and tools."

### How do I get set up? ###

It's an MVC app that needs to be automated, find the bugs for additional kudos.

### Contribution guidelines ###

* Fork this repository
* Provide automation tests using a framework and language of your choice
* We recommend using .NET C# | F#
* Provide recommendations to improve the code to ease automation.
* Let us know what you thought about your solution. if you had more time; what would you do?

### What do I do after I complete this? ###

* Once done, send us the fork
* We will assess it as soon as we can.

Thank you for taking the time to complete this test, we hope to be working with you soon :)